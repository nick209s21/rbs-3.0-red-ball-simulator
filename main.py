import time
import math
import pygame
import random
from EFFMCC import *
pygame.init()


class Particle:
    def __init__(self, pos: list, count=10, live_time=float(1), color=(200, 200, 200), end_color=None, speed=1, size=10
                 , end_size=None, gravity=(0, 0), gravity_turn=(0, 0), electric=False, circle=False, phis=False):
        if not end_size:
            end_size = size
        self.end_size = end_size
        self.pos = pos
        self.size = size
        self.count = count
        self.live_time = live_time
        self.color = color
        self.end_color = end_color
        self.speed = speed
        self.need_delete = False
        self.gravity = gravity
        self.gravity_turn = gravity_turn
        self.particles = []
        self.have_phis = phis
        for _ in range(count):
            if circle:
                angle = random.randint(0, 360)
                self.particles.append([self.pos.copy(), [math.sin(angle), math.cos(angle)], time.time()])
            else:
                self.particles.append([self.pos.copy(), [random.uniform(-1, 1), random.uniform(-1, 1)], time.time()])
            if electric:
                self.particles[-1].append([])
        pass

    def play(self, times=float(1)):
        uneror = 0
        for i, n in enumerate(self.particles.copy()):
            if time.time() - n[2] >= self.live_time:
                del self.particles[i - uneror]
                uneror += 1
            else:
                self.particles[i - uneror][0][0] += (n[1][0] * self.speed - self.gravity[0]) * times
                self.particles[i - uneror][0][1] += (n[1][1] * self.speed - self.gravity[1]) * times

                self.particles[i - uneror][1][0] += self.gravity_turn[0] * times
                self.particles[i - uneror][1][1] += self.gravity_turn[1] * times
        if not self.particles:
            self.need_delete = True

    def display(self, screen=pygame.display.set_mode((1, 1)), move=(0, 0)):
        for i in self.particles:
            if self.end_color:
                tima = (time.time() - i[2]) / self.live_time
                pygame.draw.circle(screen,
                                   ((self.color[0] + (self.end_color[0] - self.color[0]) * tima),
                                    (self.color[1] + (self.end_color[1] - self.color[1]) * tima),
                                    (self.color[2] + (self.end_color[2] - self.color[2]) * tima)),
                                   (i[0][0] - move[0], i[0][1] - move[1]),
                                   (self.size + (self.end_size - self.size) * tima))

            else:
                tima = (time.time() - i[2]) / self.live_time
                pygame.draw.circle(screen,
                                   self.color,
                                   (i[0][0] - move[0], i[0][1] - move[1]),
                                   (self.size + (self.end_size - self.size) * tima))

    def phis(self, objects=()):
        if self.have_phis:
            for i, n in enumerate(self.particles):
                for m in objects:
                    distance = math.dist(n[0], m[:2])
                    if m != n and distance <= m[2]:
                        self.particles[i][0][0] -= (m[0] - n[0][0]) / (distance / (self.speed + 1) / 2)
                        self.particles[i][0][1] -= (m[1] - n[0][1]) / (distance / (self.speed + 1) / 2)


class Player:
    def __init__(self, pos, heal=100):
        self.pos = pos
        self.heal = heal

    def damage(self, damage_min, demage_max):
        self.heal -= random.randint(damage_min, demage_max)

    def move(self, pos, speed):
        self.pos[0] += speed if pos[0] > self.pos[0] else -speed if pos[0] < self.pos[0] else 0
        self.pos[1] += speed if pos[1] > self.pos[1] else -speed if pos[1] < self.pos[1] else 0


class Barrel:
    def __init__(self, pos=(0, 0)):
        self.pos = pos
        pass


class Bullet:
    def __init__(self, pos=(0, 0), vector=(0, 0), speed=1, sharpness=0.50, mass=3):  # size=30
        self.pos = list(pos)
        self.vector = list(vector)
        self.speed = speed
        # self.mass = size  # size is size in mm
        self.sharpness = sharpness  # defeult
        self.mass = mass
        self.lsd = time.time()  # live start date (lsd)
        pass

    def move(self, times=float(1)):
        self.pos[0] += self.vector[0] * self.speed * times
        self.pos[1] += self.vector[1] * self.speed * times

    def lower_speed(self, times=float(1)):
        # self.speed *= ((self.sharpness + 1) * 60) / self.mass / self.mass / times
        self.speed -= self.mass / self.sharpness / 500 * times

    def destroy(self, sound=None):
        particles = [Particle(self.pos,
                              count=10,
                              size=3,
                              end_size=2,
                              color=(100, 100, 105),
                              end_color=(25, 25, 25))]
        if sound:
            sound.play()
        return particles


class ExplosiveMine:
    def __init__(self, pos=(0, 0), strong=20):
        self.pos = pos
        self.strong = strong
        self.stepped = False

    def explode(self):
        particles = [Particle(pos=list(self.pos),
                              size=random.randint(2, 3),
                              end_size=random.uniform(0, 2),
                              color=(random.randint(120, 230), random.randint(80, 100), 10),
                              end_color=(random.randint(140, 240), 50, 5),
                              speed=self.strong // 10,
                              gravity=(random.uniform(-1, 1),
                                       random.uniform(-1, 1)),
                              gravity_turn=(random.uniform(-0.007, 0.007),
                                            random.uniform(-0.007, 0.007)),
                              count=random.randint(3, (self.strong // 4 * 3)),
                              circle=not bool(random.randint(0, 2))) for _ in range(random.randint(3, 9))]
        return particles

    def display(self, screen=pygame.display.set_mode((100, 100))):
        pygame.draw.circle(screen, (50, 90, 50), self.pos, 5)
        redness = 4.5 * self.strong
        if redness > 250:
            redness = 250
        pygame.draw.circle(screen, (redness, 50, 50), self.pos, 2)


class EnemyBomb:
    def __init__(self, pos, heal=100, vector=(0, 0)):
        self.pos = list(pos)
        self.heal = heal
        self.vector = list(vector)
        pass

    def move(self, target, times=float(1), randomness=0.01):

        distance_x = self.pos[0] - target[0]
        distance_y = self.pos[1] - target[1]

        angle = math.atan2(distance_y, distance_x)

        speed_x = math.cos(angle)
        speed_y = math.sin(angle)

        self.pos[0] -= self.vector[0] * times
        self.pos[1] -= self.vector[1] * times

        self.vector[0] += (0.005 if speed_x > self.vector[0] else -0.005 if speed_x < self.vector[0] else 0) * times
        self.vector[1] += (0.005 if speed_y > self.vector[1] else -0.005 if speed_y < self.vector[1] else 0) * times

        self.vector[0] += random.uniform(-randomness, randomness) * times
        self.vector[1] += random.uniform(-randomness, randomness) * times

        # self.vector = [-speed_x, -speed_y]

    def explode(self):
        particles = [Particle(list(self.pos),
                              count=10,
                              size=5,
                              end_size=3,
                              color=(200, 150, 0),
                              end_color=(200, 50, 50),
                              speed=2)]
        time.sleep(0.001)
        particles.append(Particle(list(self.pos),
                                  count=10,
                                  size=5,
                                  end_size=2,
                                  color=(200, 150, 0),
                                  end_color=(200, 50, 50),
                                  live_time=1,
                                  speed=2))
        return particles


def cellilade(a, cell):
    return (a[0] // cell * cell,
            a[1] // cell * cell)


def center(a):
    musas = []
    misas = []
    for i in a:
        for m, n in enumerate(i):
            if m < len(musas):
                musas[m] += n
                misas[m] += 1
            else:
                musas.append(n)
                misas.append(1)
    pisas = []
    for i, n in enumerate(musas):
        pisas.append(n / misas[i])
    return pisas


def can_hit(speed=7, sharpness=0.5, mass=3, dist=float(1), min_speed=2):
    b = speed
    for i in range(int(dist)):
        if b:
            b -= 1
            continue
        else:
            speed -= mass / sharpness / 500
            b = int(speed)
    return speed > min_speed


def calculate_angle(a, b):
    dx = a[1] - b[0]
    dy = b[1] - a[0]
    rads = math.atan2(-dy, dx)
    rads %= 2 * math.pi
    degs = math.degrees(rads)
    return degs


def main_game(screen=pygame.display.set_mode((1920, 1080), pygame.FULLSCREEN)):

    def smoke(fog=False, real_smoke=False, phis_smoke=False):
        if phis_smoke:
            particles.append(Particle([500, 500],
                                      count=20,
                                      size=3,
                                      end_size=2,
                                      color=(100, 100, 105),
                                      end_color=(125, 125, 125),
                                      phis=True,
                                      live_time=3,
                                      circle=True,
                                      speed=1))
        if real_smoke:
            particles.append(Particle([500, 500],
                                      count=20,
                                      size=10,
                                      end_size=4,
                                      color=(100, 100, 105),
                                      end_color=(125, 125, 125),
                                      phis=True,
                                      live_time=3,
                                      circle=not random.randint(0, 1),
                                      speed=1,
                                      gravity=(random.uniform(-1, 1),
                                               random.uniform(-1, 1))))
        if fog:
            particles.append(Particle([-10, random.randint(10, screen.get_width() - 10)],
                                      count=5,
                                      size=random.randint(3, 10),
                                      end_size=random.randint(1, 12),
                                      color=(100, 100, 105),
                                      end_color=(125, 125, 125),
                                      phis=True,
                                      live_time=14,
                                      circle=False,
                                      speed=1,
                                      gravity=(-1,
                                               0),
                                      gravity_turn=(0.004,
                                                    0)))

    def shooting(gun: dict, place=(0, 0), vector=(0, 0)):
        for _ in range(player_gun['bullet count']):
            bullets.append(Bullet(pos=(place[0] + vector[0] * gun['bullet launch dist'],
                                       place[1] + vector[1] * gun['bullet launch dist']),
                                  vector=(vector[0]
                                          + random.uniform(-gun['randomness_now'],
                                                           gun['randomness_now']),
                                          vector[1] + random.uniform(-gun['randomness_now'],
                                                                     gun['randomness_now'])),
                                  speed=gun['bullet speed'],
                                  mass=gun['bullet mass'],
                                  sharpness=gun['bullet sharpness']))
        # bullets[-1].move(2)
        shoot.set_volume(0.08)
        shoot.play()
        particles.append(Particle(pos=bullets[-1].pos,
                                  gravity=(-vector[0] * 5, -vector[1] * 5),
                                  live_time=0.2,
                                  speed=3,
                                  count=random.randint(2, 10),
                                  size=4,
                                  end_size=2,
                                  color=(255, 242, 0),
                                  end_color=(150, 100, 90)))
        player_gun['last shoot'] = time.time()
        if player_gun['randomness rate roof'] > player_gun['randomness rate']:
            player_gun['randomness rate'] += player_gun['randomness rate roof']

    def explosion(en):
        if math.dist(players.pos, en.pos) <= 100:
            players.damage(30, 70)
        particles.append(Particle(list(i.pos),
                                  count=20,
                                  size=5,
                                  end_size=3,
                                  color=(200, 150, 0),
                                  end_color=(200, 50, 50),
                                  speed=2))
        time.sleep(0.001)
        particles.append(Particle(list(i.pos),
                                  count=20,
                                  size=5,
                                  end_size=2,
                                  color=(200, 150, 0),
                                  end_color=(200, 50, 50),
                                  live_time=1,
                                  speed=2))

        particles.append(Particle(list(en.pos),
                                  count=20,
                                  size=5,
                                  end_size=3,
                                  color=(200, 150, 0),
                                  end_color=(200, 50, 50),
                                  speed=5))
        time.sleep(0.001)
        particles.append(Particle(list(en.pos),
                                  count=20,
                                  size=5,
                                  end_size=2,
                                  color=(200, 150, 0),
                                  end_color=(200, 50, 50),
                                  live_time=1,
                                  speed=5))
        time.sleep(0.005)
        particles.append(Particle(list(en.pos),
                                  count=20,
                                  size=5,
                                  end_size=2,
                                  color=(200, 150, 0),
                                  end_color=(200, 50, 50),
                                  live_time=1,
                                  speed=5))
        explosionsound.set_volume(0.1)
        explosionsound.play()
    clock = pygame.time.Clock()

    # particles = [Particle([500, 500], count=10, live_time=1, color=(200, 150, 0),
    #                       speed=5, size=5, end_color=(200, 50, 50))]
    particles = []
    wind = 0
    showdebug = False
    max_particles = 10000
    players = Player([screen.get_size()[0] // 2, screen.get_size()[1] // 2], 100)
    bullets = []
    playersmoveto = players.pos
    prev_time = time.time()
    fps = 60  # please do not go forward more than 60 or less then 30
    stamina = 1000
    explosionsound = pygame.mixer.Sound('audio/redactingvoice.wav')
    shoot = pygame.mixer.Sound('audio/vistrel_odin.wav')
    bulletdestroy = pygame.mixer.Sound('audio/bulletend.wav')
    bulletdestroy.set_volume(0.03)

    barrels = []  # Barrel((500, 500))
    enemies = []
    advance_enemies = []
    for _ in range(1):
        advance_enemies.append(EnemySecond([random.randint(0, screen.get_size()[0]),
                                            random.randint(0, screen.get_size()[1])],
                                           speed=1))
        advance_enemies[-1].attach_player(players)

    # particles.append(Particle([500, 500],
    #                           count=1000,
    #                           size=3,
    #                           end_size=2,
    #                           color=(100, 100, 105),
    #                           end_color=(125, 125, 125),
    #                           phis=True,
    #                           live_time=float('inf'),
    #                           circle=False,
    #                           speed=100))
    # particles[-1].play(times=1)
    # particles[-1].speed = 0

    for _ in range(random.randint(1, 3)):
        enemies.append(EnemyBomb((random.randint(0, screen.get_size()[0]),
                                 random.randint(0, screen.get_size()[1]))))

    for _ in range(random.randint(1, 10)):
        barrels.append(Barrel((random.randint(0, screen.get_size()[0]),
                               random.randint(0, screen.get_size()[1]))))
    for _ in range(random.randint(1, 5)):
        enemies.append(ExplosiveMine(pos=(random.randint(0, screen.get_size()[0]),
                                          random.randint(0, screen.get_size()[1])),
                                     strong=random.randint(10, 40)))
    hellmode = False
    pygame.mouse.set_visible(False)
    player_gun = {'bullet mass': 3,
                  'bullet sharpness': 0.20,
                  'bullet speed': 7,
                  'randomness': 0.05,
                  'randomness_now': 0.05,
                  'bullet count': 1,
                  'bullet launch dist': 25,
                  'name': 'gun',
                  'texture': pygame.image.load('images/gun.png'),
                  'time between shoot': 0.4,
                  'last shoot': 0,
                  'allow fast shoot': False,
                  'randomness rate': 0,
                  'randomness rate reload speed': 0.05,
                  'randomness rate higher': 0.06,
                  'randomness rate roof': 0.1}
    scaling_mouse = True
    sprinting = False
    deactivating = 0
    while True:
        can_destroy = False
        countparticles = 0
        dt = time.time() - prev_time
        prev_time = time.time()
        get_time_delta = 64 * dt

        wanted_randomness = player_gun['randomness'] * (sprinting + 1) * ((players.pos != playersmoveto) + 1)
        wanted_randomness += player_gun['randomness rate']
        player_gun['randomness_now'] += (0.1 if player_gun['randomness_now'] < wanted_randomness
                                         else
                                         -0.1 if player_gun['randomness_now'] > wanted_randomness
                                         else 0) * ((0 < player_gun['randomness rate']) + 1) * dt
        if abs(wanted_randomness - player_gun['randomness_now']) <= 0.001:
            player_gun['randomness_now'] = wanted_randomness
        if player_gun['randomness rate'] > 0:
            player_gun['randomness rate'] -= player_gun['randomness rate reload speed']
        if player_gun['randomness rate'] < player_gun['randomness rate reload speed']:
            player_gun['randomness rate'] = 0

        mouse_x, mouse_y = pygame.mouse.get_pos()

        distance_x = mouse_x - players.pos[0]
        distance_y = mouse_y - players.pos[1]

        angle = math.atan2(distance_y, distance_x)

        speed_x = math.cos(angle)
        speed_y = math.sin(angle)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
            if event.type == pygame.KEYDOWN:
                # print(event)
                if event.key == 61:
                    wind += 0.01
                if event.key == 45:
                    wind -= 0.01
                if event.key == 48:
                    wind = 0
                if event.key == 100:
                    if showdebug:
                        showdebug = False
                    else:
                        showdebug = True
            if event.type == pygame.MOUSEBUTTONDOWN:
                if pygame.mouse.get_pressed()[2]:
                    particles.append(Particle(pos=list(playersmoveto),
                                              count=random.randint(0, 5),
                                              speed=1,
                                              size=1,
                                              end_size=0,
                                              live_time=0.3,
                                              color=(50, 200, 50),
                                              end_color=(25, 100, 25)))
                    playersmoveto = (pygame.mouse.get_pos()[0] // 2 * 2,
                                     pygame.mouse.get_pos()[1] // 2 * 2)
                if pygame.mouse.get_pressed()[0]:
                    if time.time() - player_gun['last shoot'] >= player_gun['time between shoot']:
                        shooting(player_gun, players.pos, (speed_x, speed_y))

        if pygame.mouse.get_pressed()[0]:
            if player_gun['allow fast shoot'] and\
                    time.time() - player_gun['last shoot'] >= player_gun['time between shoot']:
                shooting(player_gun, players.pos, (speed_x, speed_y))

        key = pygame.key.get_pressed()
        if key[pygame.K_RSHIFT] or key[pygame.K_LSHIFT]:
            if stamina > 0:
                sprinting = True
                stamina -= 5 * get_time_delta
            else:
                sprinting = False
        else:
            sprinting = False
            if stamina < 1000:
                stamina += 10 * get_time_delta
        for i in particles.copy():
            # print(dt, 1 // 0.015568733215332031 * dt, clock.get_fps(), 1 // 0.015568733215332031)
            i.play(times=get_time_delta)
            i.display(screen)
            if i.have_phis:
                i.phis([(*players.pos, 15),
                        *((*j.pos,
                           20) for j in barrels),
                        *((*j.pos,
                           5) for j in enemies),
                        *((*j.pos,
                           j.mass) for j in bullets)])  # *(((*j[0], j[2]) for j in k.particles) for k in particles)]
            if i.need_delete:
                particles.remove(i)
            countparticles += len(i.particles)
        if countparticles >= 100000:
            del particles[0]
        for i in advance_enemies[:]:
            i.display(screen)
            i.turn(get_time_delta)
            i.move(get_time_delta)
            s = i.shoot()
        for i in enemies[:]:
            if isinstance(i, EnemyBomb):
                i.move(target=players.pos, times=(4 - (0.5 * (i.heal <= 40))) * get_time_delta,
                       randomness=(0.01 * (1 + (i.heal <= 40) * 2)))
                pygame.draw.circle(screen, (90, 90, 110), (i.pos[0] - i.vector[0], i.pos[1] - i.vector[1]), 6)
                pygame.draw.circle(screen, (100, 100, 100), i.pos, 6)
                if i.heal > 40:
                    pygame.draw.circle(screen, (50, 50, 150), i.pos, 2)
                    particles.append(Particle(i.pos,
                                              count=1,
                                              size=1,
                                              color=(80, 80, 100),
                                              end_color=(100, 80, 80),))
                else:
                    pygame.draw.circle(screen, (150, 50, 50), i.pos, 2)
                    particles.append(Particle(i.pos,
                                              count=9,
                                              size=1,
                                              color=(60, 55, 50),
                                              end_color=(50, 40, 40),
                                              gravity=(random.uniform(-1, 1), random.uniform(-1, 1))))
                for n in bullets[:]:
                    if math.dist(n.pos, i.pos) <= 6 + n.mass:
                        bullet_power = int(n.mass * n.sharpness * n.speed)
                        i.heal -= random.randint(bullet_power + 15, bullet_power + 65)
                        particles.extend(n.destroy())
                        bullets.remove(n)
                        continue
                if i.heal <= 0:
                    particles.extend(i.explode())
                    enemies.remove(i)
                    continue
                if math.dist(i.pos, players.pos) <= 21:
                    enemies.remove(i)
                    players.damage(30, 50)
                    particles.extend(i.explode())
                    break
                else:
                    for n in barrels:
                        if math.dist(i.pos, n.pos) <= 24:
                            explosion(n)
                            barrels.remove(n)
                            enemies.remove(i)
                            break
            elif isinstance(i, ExplosiveMine):
                i.display(screen)
                if math.dist(i.pos, players.pos) <= 12:
                    i.stepped = True
                    can_destroy = True
                    if deactivating >= 100:
                        deactivating = 0
                        particles.extend(i.explode())
                        enemies.remove(i)
                    elif key[pygame.K_e]:
                        deactivating += get_time_delta
                        if not random.randint(0, int(10 * get_time_delta)):
                            particles.append(Particle(pos=list(i.pos),
                                                      count=random.randint(1, 2),
                                                      color=(random.randint(30, 50),
                                                             random.randint(30, 50),
                                                             random.randint(100, 200)),
                                                      size=random.randint(2, 3),
                                                      end_size=1))
                    else:
                        deactivating = 0
                elif i.stepped:
                    particles.extend(i.explode())
                    if math.dist(i.pos, players.pos) <= 30:
                        players.damage(i.strong // 2, i.strong * 2)
                    enemies.remove(i)
            pass
        if countparticles > max_particles:
            del particles[:2]
        for n in barrels[:]:
            pygame.draw.circle(screen, (50, 50, 50), n.pos, 18)
            for i in bullets[:]:
                if math.dist(i.pos, n.pos) <= 18 + i.mass:
                    explosion(n)
                    barrels.remove(n)
                    bullets.remove(i)
                    break
        dd = 0  # debug delete
        for i, n in enumerate(bullets[:]):
            if -100 < n.pos[0] < screen.get_size()[0] + 100\
                    and -100 < n.pos[1] < screen.get_size()[1] + 100 and n.speed > 2:
                n.move(times=1 * get_time_delta)
                n.lower_speed(times=1 * get_time_delta)
                pygame.draw.circle(screen, (100, 100, 100), n.pos, n.mass)
            else:
                particles.extend(n.destroy(bulletdestroy))
                # bulletdestroy.play()
                del bullets[i - dd]
                dd += 1
                if math.dist(n.pos, players.pos) <= 15:
                    bullet_power = int(n.mass * n.sharpness * n.speed)
                    players.damage(bullet_power / 2, bullet_power * 2)
        # print(dt)
        players.move(playersmoveto, (200 + sprinting * 100) * dt)

        if math.dist([players.pos[0]], [playersmoveto[0]]) <= 3 * get_time_delta:
            players.pos[0] = playersmoveto[0]

        if math.dist([players.pos[1]], [playersmoveto[1]]) <= 3 * get_time_delta:
            players.pos[1] = playersmoveto[1]
        # players.pos = list(cellilade(players.pos,
        #                              2))
        pygame.draw.circle(screen, (20, 200, 25), playersmoveto, 5)

        if random.uniform(0.001, 10) <= 0.5 * (sprinting * 4 + 1) * get_time_delta and\
                players.pos != list(playersmoveto):
            particles.append(Particle(players.pos,
                                      count=1,
                                      size=3,
                                      end_size=2,
                                      color=(100, 100, 105),
                                      end_color=(25, 25, 25)))

        # pygame.draw.circle(screen, (50, 51, 52), (players.pos[0] + 6, players.pos[1] + 1), 15)
        # image = pygame.transform.scale(pygame.image.load('images/shadow.png'), (30, 30))
        # screen.blit(image, (players.pos[0] + 8 - 7.5, players.pos[1] + 1 - 7.5))
        if player_gun['texture']:
            image = player_gun['texture']
            image = pygame.transform.scale(image, (player_gun['bullet launch dist'] * 2, image.get_height()))
            image = pygame.transform.flip(pygame.transform.rotate(image,
                                                                  math.degrees(angle)), flip_x=False, flip_y=True)
            screen.blit(image, (players.pos[0] - image.get_width() / 2,
                                players.pos[1] - image.get_height() / 2))
        else:
            pygame.draw.line(screen, (80, 80, 80), players.pos, (players.pos[0] + speed_x
                                                                 * player_gun['bullet launch dist'],
                                                                 players.pos[1] + speed_y
                                                                 * player_gun['bullet launch dist']), 10)

        pygame.draw.circle(screen, (200, 20, 25), players.pos, 15, int((players.heal / 100 * 15)))
        if deactivating:
            pygame.draw.line(screen, (150, 150, 150), (players.pos[0] - deactivating / 5,
                                                       players.pos[1] + 20),
                             (players.pos[0] + deactivating / 5,
                              players.pos[1] + 20), 5)
            # pygame.draw.line(screen, (150, 150, 150), (players.pos[0] - (100 - deactivating) / 5,
            #                                            players.pos[1] + 20),
            #                  (players.pos[0] + (100 - deactivating) / 5,
            #                   players.pos[1] + 20), 5)
        if hellmode:
            particles.append(Particle([random.randint(0, screen.get_size()[0]),
                                       random.randint(0, screen.get_size()[1])],
                                      count=10,
                                      size=5,
                                      end_size=3,
                                      color=(200, 150, 0),
                                      end_color=(200, 50, 50),
                                      speed=5))
            time.sleep(0.001)
            particles.append(Particle([random.randint(0, screen.get_size()[0]),
                                       random.randint(0, screen.get_size()[1])],
                                      count=10,
                                      size=5,
                                      end_size=2,
                                      color=(200, 150, 0),
                                      end_color=(200, 50, 50),
                                      live_time=1,
                                      speed=5))
            time.sleep(0.005)
            particles.append(Particle([random.randint(0, screen.get_size()[0]),
                                       random.randint(0, screen.get_size()[1])],
                                      count=10,
                                      size=5,
                                      end_size=2,
                                      color=(200, 150, 0),
                                      end_color=(200, 50, 50),
                                      live_time=1,
                                      speed=5))
        font = pygame.font.SysFont('Bodoni MT', 25, bold=False, italic=False)
        toxt = font.render(f'fps:{int(clock.get_fps())}', True, (200, 200, 200))
        screen.blit(toxt, dest=(10, screen.get_size()[1] - 20))
        toxt = font.render(f'particles:{countparticles}', True, (200, 200, 200))
        screen.blit(toxt, dest=(75, screen.get_size()[1] - 20))
        if can_destroy:
            font = pygame.font.SysFont('Bodoni MT', int(screen.get_size()[0] / screen.get_size()[1] * 45),
                                       bold=False, italic=False)
            toxt = font.render(f'hold E to deactivate the explosive mine', True, (200, 200, 200))
            screen.blit(toxt, dest=(screen.get_width() // 4,
                                    screen.get_size()[1] - screen.get_size()[0] // screen.get_size()[1] * 55))

        bar_progress = (time.time() - player_gun['last shoot']) * 100 / player_gun['time between shoot']
        if bar_progress > 100:
            bar_progress = 100

        pygame.draw.line(screen,
                         (50, 150, 100),
                         (0, 10),
                         (bar_progress * 2, 10),
                         5)

        pygame.draw.line(screen, (50, 50, 200), (0, 5), (stamina / 5, 5), 5)

        pos = pygame.mouse.get_pos()
        mouse = pygame.image.load('images/mouse3.png')
        if scaling_mouse:
            m_size = player_gun['randomness_now'] * (math.dist(pos, players.pos) - player_gun['bullet launch dist'])\
                     + player_gun['bullet mass']
            if can_hit(speed=player_gun['bullet speed'],
                       sharpness=player_gun['bullet sharpness'],
                       mass=player_gun['bullet mass'],
                       dist=math.dist(players.pos, pos) + m_size,
                       min_speed=2):
                pygame.draw.circle(screen,
                                   (100, 200, 50),
                                   pos,
                                   m_size,
                                   1)
            else:
                pygame.draw.circle(screen,
                                   (200, 100, 50),
                                   pos,
                                   m_size,
                                   1)
        screen.blit(mouse, (pos[0] - mouse.get_width() / 2,
                            pos[1] - mouse.get_height() / 2))
        if not can_destroy:
            deactivating = 0
        smoke(phis_smoke=False, real_smoke=False, fog=False)
        pygame.display.flip()
        clock.tick(fps)
        screen.fill((30, 31, 32))
        # background = pygame.transform.scale(pygame.image.load('images/background.png'), screen.get_size())
        # screen.blit(background, (0, 0))
    pass


if __name__ == '__main__':
    main_game()
