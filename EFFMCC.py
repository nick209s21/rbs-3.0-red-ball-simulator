# ENEMY FILE FOR MORE CLEAN CODE
import math
import time

import pygame.display

from main import *


class EnemyTemplate:
    def __init__(self,
                 pos=(0, 0),
                 speed=5,
                 heal=100,
                 power=10,
                 shoot_interval=1,
                 turn_speed=1,
                 have_velocity=False):
        self.speed = speed
        self.heal = heal
        self.last_attack = 0
        self.power = power
        self.shoot_interval = shoot_interval
        self.player = Player((0, 0))
        self.pos = list(pos)
        self.angle = 0
        self.have_velocity = have_velocity
        self.velocity = [0, 0]
        self.turn_speed = turn_speed

    def phis_move(self, times=1):
        """
        useful only if it have velocity
        """
        self.pos[0] += self.velocity[0] * self.speed * times
        self.pos[1] += self.velocity[1] * self.speed * times

    def move(self, times=1):
        angle = math.radians(self.angle)
        if self.have_velocity:
            self.velocity[0] += math.sin(angle) * self.speed * times
            self.velocity[1] += math.cos(angle) * self.speed * times
        else:
            self.pos[0] += math.sin(angle) * self.speed * times
            self.pos[1] += math.cos(angle) * self.speed * times

    def turn(self, times=1, target=None):
        if target:
            angle = calculate_angle(self.pos, target)
        elif self.player:
            angle = calculate_angle(self.pos, self.player.pos)
        else:
            angle = self.angle
        angle = int(angle)
        print(angle, self.angle)
        self.angle += (1 if angle > self.angle else -1 if angle < self.angle else 0) * times
        self.angle = abs(self.angle - 360) if self.angle > 360 else (self.angle + 360 if self.angle < 0 else self.angle)

    def attach_player(self, player: Player):
        self.player = player

    def can_attack(self, angle=30):

        if self.last_attack - time.time() >= self.shoot_interval and\
                abs(self.angle - calculate_angle(self.pos, self.player.pos)) <= angle:
            return True
        else:
            return False
        pass


class EnemySecond(EnemyTemplate):
    def display(self, screen=pygame.display.set_mode((100, 100)), size=float(1)):
        # pygame.draw.circle(screen, (210, 49, 183), self.player.pos, 100 * size)
        pygame.draw.line(screen,
                         (100, 100, 100),
                         self.pos,
                         (self.pos[0] + math.sin(math.radians(self.angle)) * 15 * size,
                          self.pos[1] + math.cos(math.radians(self.angle)) * 15 * size),
                         int(3 * size))
        pygame.draw.circle(screen, (210, 49, 183), self.pos, 10 * size)
        pygame.draw.circle(screen, (183, 49, 210), self.pos, 4 * size)

    def explode(self):
        particles = []
        for i in range(random.randint(2, 5)):
            particles.append(Particle(self.pos,
                                      count=10,
                                      color=(200, 100, 130),
                                      end_color=(150, 50, 50),
                                      live_time=1,
                                      end_size=2,
                                      size=4,
                                      speed=3,
                                      circle=i % 2))
        return particles

    def shoot(self, size=1):
        particles = []
        bullets = []
        if math.dist(self.pos, self.player.pos) <= 300 * size:
            if self.can_attack(50):
                self.last_attack = time.time()
                bullets.append(Bullet(pos=[self.pos[0] * math.sin(math.radians(self.pos[0])) * 10 * size,
                                           self.pos[1] * math.cos(math.radians(self.pos[1])) * 10 * size],
                                      mass=3,
                                      speed=10,
                                      sharpness=0.5,
                                      vector=[math.sin(math.radians(self.angle)),
                                              math.cos(math.radians(self.angle))]))
                for i in range(random.randint(2, 3)):
                    particles.append(Particle([self.pos[0] * math.sin(math.radians(self.pos[0])) * 10 * size,
                                               self.pos[1] * math.cos(math.radians(self.pos[1])) * 10 * size],
                                              count=10,
                                              color=(200, 100, 130),
                                              end_color=(150, 50, 50),
                                              live_time=1,
                                              end_size=2,
                                              size=4,
                                              speed=3,
                                              circle=i % 2,
                                              gravity=[math.sin(math.radians(self.angle)) * 4,
                                                       math.cos(math.radians(self.angle) * 4)],
                                              ))
        return [particles, bullets]


